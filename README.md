# PostgresSQL_on_K8



## Deploy PostgreSQL by Creating Configuration from Scratch

### Prerequisites

        - A Kubernetes cluster with kubectl installed
        - Administrative privileges on your system

### 1 Apply ConfigMap

        kubectl apply -f postgres-configmap.yaml


### 2 Apply Persistent Storage Volume and Persistent Volume Claim

        kubectl apply -f postgres-storage.yaml

### 3 Apply PostgreSQL Deployment

        kubectl apply -f postgres-deployment.yaml

### 4 Apply PostgreSQL Service

        kubectl apply -f postgres-service.yaml

### 5 Use the following command to list all resources on the system.

        kubectl get all

### 6 Connect to PostgreSQL

        kubectl exec -it [pod-name] --  psql -h localhost -U admin --password -p [port] postgresdb
